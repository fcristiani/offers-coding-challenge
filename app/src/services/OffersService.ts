import axios, { AxiosInstance } from 'axios';

const baseURL = "http://localhost:9000";

export interface OfferListing {
	id: string;
	title: string;
	image: string;
	price: number;
}

export interface Offer {
	id: string;
	title: string;
	price: number;
	conditions: {
		minimumAge: number;
		maximumAge: number;
		minLicenseDuration: number;
	};

	equipmentDetails: {
		name: string;
	}[];
}

export class OffersService {
	private service: AxiosInstance;

	constructor() {
		this.service = axios.create({
			baseURL
		});
	}

	async list(): Promise<OfferListing[]> {
		const response = await this.service.get("/offers");

		return response.data.offers.map((offer: any) => ({
			id: offer.id,
			title: offer.teaser.title,
			image: offer.teaser.teaserImage,
			price: offer.pricing.price
		}));
	}

	async get(id: string): Promise<Offer> {
		const response = await this.service.get("/offers/" + id);
		const offer = response.data.offer;

		return {
			id: offer.id,
			title: offer.teaser.title,
			price: offer.pricing.price,
			conditions: offer.conditions,
			equipmentDetails: offer.car.equipmentDetails
		};
	}
}