import Vue from 'vue'
import VueRouter from 'vue-router'

import AvailableOffersList from '../components/AvailableOffersList.vue'
import OfferDetailsPage from '../components/OfferDetails.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      component: AvailableOffersList,
      meta: {
        title: 'Our offers'
      }
    },
    { 
      path: '/offers/:id',
      component: OfferDetailsPage
    },
    { path: '*', redirect: '/' }
  ]
});

export default router