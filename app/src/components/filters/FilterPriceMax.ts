import { OfferListing } from '../../services/OffersService';

import Filter from './Filter';

export default class FilterPriceMax implements Filter {
	private filterValue = "";

	get filterFunction(): (o: OfferListing) => boolean {
		return (o: OfferListing) => {
			const value = parseInt(this.value);
			
			if(isNaN(value) === true) {
				return true;
			}
			
			return (o.price <= value);
		};
	}

	set value(v: string) {
		this.filterValue = v;
	}

	get value(): string {
		return this.filterValue;
	}
}