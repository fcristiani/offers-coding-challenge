import { OfferListing } from '../../services/OffersService';

export default interface Filter {
	filterFunction: (o: OfferListing) => boolean;
	value: string;
}