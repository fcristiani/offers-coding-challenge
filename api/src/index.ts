import express from 'express';
import OffersRoute from './routes/OffersRoute';

import OffersClient from './clients/OffersClient';

const PORT = 9000;
const offersClient = new OffersClient();
const offersRoute = new OffersRoute(offersClient);

const app = express()
	.use(express.json())
	.use(function (req, res, next) {
		res.header("Access-Control-Allow-Origin", "*");
		next();
	});

offersRoute.apply(app);

console.log("Loading mock data ...");
offersClient.loadData();

app.listen(PORT, () => {
	console.log(`Server is listening on port ${PORT}`);
});