import express, { Request, Response } from 'express';
import OffersClient, { Offer } from '../clients/OffersClient';

import FilteringOffer from '../filters/FilteringOffer';
import FilterPriceMin from '../filters/FilterPriceMin';
import FilterPriceMax from '../filters/FilterPriceMax';
import FilterAlwaysVisible from '../filters/FilterAlwaysVisible';

export default class OffersRoute {
	private client: OffersClient;
	private basePath = "/offers";
	private filteringOffer: FilteringOffer;

	constructor(client: OffersClient) {
		this.client = client;
		this.filteringOffer = new FilteringOffer()
			.addFilter(new FilterPriceMin())
			.addFilter(new FilterPriceMax())
			.addFilter(new FilterAlwaysVisible());
	}

	apply(app: express.Express) {
		app.use(this.basePath, this.createRouter());
	}

	createRouter() {
		let router = express.Router({
			strict: true
		});

		router.get('/', (req: Request, res: Response) => {
			res.status(200);

			let offers = this.client.list(
				(o: Offer) => {
					return this.filteringOffer.apply(o, req.query)
				},
				(a, b) => {
					let priceA = a.pricing.price;
					let priceB = b.pricing.price;

					if (priceA > priceB) {
						return 1;
					}

					if (priceB > priceA) {
						return -1;
					}

					return 0;
				}
			);

			return res.json({
				count: offers.length,
				offers: offers
			});
		})

		router.get('/:id(\\d+)', (req: Request, res: Response) => {
			let id = req.params.id;
			
			let offer = this.client.get(id);
			
			if (offer === null) {
				res.status(404);
				return res.send({
					message: "Offer with id " + id + " does not exist"
				});				
			}

			res.status(200);
			return res.send({
				offer: offer
			});
		});

		return router;
	}
}