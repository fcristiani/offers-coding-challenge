import aws from 'aws-sdk';
import fs from 'fs';

export interface Offer {
	id: string,
	[k: string]: any
}

export default class OffersClient {
	private offers: { [id: string]: Offer } = {};

	loadData() {
		JSON.parse(fs.readFileSync('./data/dynamodb.export.json').toString()).Items.forEach(dynamoDbItem => {
			let offer = aws.DynamoDB.Converter.unmarshall(dynamoDbItem) as Offer;
			this.addOffer(offer);
		});
	}

	addOffer(offer: Offer) {
		this.offers[offer.id] = offer;
	}

	list(filter?: (offer: Offer) => boolean, sort?: (a: Offer, b: Offer) => number): Offer[] {
		if (!filter) {
			filter = () => true;
		}

		if (!sort) {
			sort = () => 0;
		}

		return Object.values(this.offers)
			.filter(filter)
			.sort(sort);
	}

	get(id: string) : Offer | null {
		return this.offers[id] || null;
	}
}