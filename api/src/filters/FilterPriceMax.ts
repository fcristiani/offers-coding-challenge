import { Offer } from '../clients/OffersClient';
import FilterData from './FilterData';
import BaseFilterOffer from './BaseFilterOffer';

export default class FilterPriceMax extends BaseFilterOffer {
	getCode() {
		return "price_max";
	}

	apply(offer: Offer, filterData: FilterData) : boolean {
		let filterValue = parseInt(this.getFilterValue(filterData));

		if (isNaN(filterValue) === true) {
			return true;
		}

		return (offer.pricing.price <= filterValue);
 	}
}