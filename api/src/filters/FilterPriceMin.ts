import { Offer } from '../clients/OffersClient';
import FilterData from './FilterData';
import FilterOffer from './FilterOffer';
import BaseFilterOffer from './BaseFilterOffer';

export default class FilterPriceMin extends BaseFilterOffer {
	getCode() {
		return "price_min";
	}

	apply(offer: Offer, filterData: FilterData) : boolean {
		let filterValue = parseInt(this.getFilterValue(filterData));

		if (isNaN(filterValue) === true) {
			return true;
		}

		return (offer.pricing.price >= filterValue);
 	}
}