import FilterOffer from './FilterOffer';
import { Offer } from '../clients/OffersClient';
import FilterData from './FilterData';

export default class FilteringOffer implements FilterOffer {
	private filters: FilterOffer[] = [];

	addFilter(filter: FilterOffer) {
		this.filters.push(filter);
		return this;
	}

	apply(offer: Offer, filterData: FilterData) : boolean {
		let filterResult = true;
		this.filters.forEach(f => {
			if (f.apply(offer, filterData) === false) {
				filterResult = false;
				return;
			}
		});

		return filterResult;
	}
}