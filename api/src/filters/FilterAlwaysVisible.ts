import { Offer } from '../clients/OffersClient';
import FilterData from './FilterData';
import BaseFilterOffer from './BaseFilterOffer';

export default class FilterPriceMax extends BaseFilterOffer {
	getCode() {
		return "visible_true";
	}

	apply(offer: Offer, filterData: FilterData) : boolean {
		return (offer.visible === true);
 	}
}