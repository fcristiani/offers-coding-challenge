export default interface FilterData {
	[k: string]: any
}