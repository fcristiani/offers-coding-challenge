import { Offer } from '../clients/OffersClient';
import FilterData from './FilterData';
import FilterOffer from './FilterOffer';

export default abstract class BaseFilterOffer implements FilterOffer {
	abstract getCode(): string;
	abstract apply(offer: Offer, filterData: FilterData): boolean;

	getFilterValue(filterData: FilterData) {
		return filterData[this.getCode()];
	}
}