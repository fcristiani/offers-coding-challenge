import { Offer } from '../clients/OffersClient';
import FilterData from './FilterData';

export default interface FilterOffer {
	apply(offer: Offer, filetrData: FilterData) : boolean;
}