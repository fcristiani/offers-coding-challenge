# offers-coding-challenge


## Build & Run
Start the app and the api with `docker-compose` and then open `http://localhost:8081` in your browser. The Rest API can be accessed under `http://localhost:9000`.
```
$ docker-compose up -d
```

You can stop the app with:
```
$ docker-compose down
```

## RestAPI
* Express api written with TypeScript.
* Mock data gets loaded in memory at service initialization. The provided DynamoDB Export file was commited to this repo and transformed into a readable JSON using the AWS sdk.
* Endpoints /offers and /offers/:id were implemented.
* Filtering was also added to the Rest API. Only price_min and price_max filters are supported.
* Only offers with 'visible = true' are returend.
* The endpoint /offers returns a list of offers that is ordered ascending by price.

## App
* Vue.js app created with vue-cli.
* Only some data is shown in the 'details' view. Adding more data would be trivial.